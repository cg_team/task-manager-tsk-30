package ru.inshakov.tm.component;

import lombok.SneakyThrows;
import ru.inshakov.tm.bootstrap.Bootstrap;

public class Backup extends Thread {

    private final Bootstrap bootstrap;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(5000);
        }
    }

    public void init() {
        load();
        start();
    }

    @SneakyThrows
    public void save() {
        bootstrap.parseCommand("backup-save");
    }

    @SneakyThrows
    public void load() {
        bootstrap.parseCommand("backup-load");
    }

}
