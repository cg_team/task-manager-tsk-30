package ru.inshakov.tm.command;

import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.model.Project;

import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand{

    protected void showProject(final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
    }

    public Role[] roles() {
        return Role.values();
    }

}
