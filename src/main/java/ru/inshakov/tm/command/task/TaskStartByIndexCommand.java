package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-start-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "start task by index";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Task task = serviceLocator.getTaskService().startByIndex(userId, index);
    }

}
