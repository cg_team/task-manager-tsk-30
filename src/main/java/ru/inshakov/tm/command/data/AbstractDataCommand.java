package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.dto.Domain;
import ru.inshakov.tm.exception.empty.EmptyDomainException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.model.User;

import java.util.List;
import java.util.Optional;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public Domain getDomain() {
        final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        Optional.ofNullable(domain).orElseThrow(EmptyDomainException::new);
        serviceLocator.getTaskService().clear();
        @Nullable final List<Task> tasks = domain.getTasks();
        if (tasks != null) serviceLocator.getTaskService().addAll(tasks);
        serviceLocator.getTaskService().clear();
        @Nullable final List<Project> projects = domain.getProjects();
        if (projects != null) serviceLocator.getProjectService().addAll(projects);
        serviceLocator.getTaskService().clear();
        @Nullable final List<User> users = domain.getUsers();
        if (users != null) serviceLocator.getUserService().addAll(users);
        serviceLocator.getAuthService().logout();
    }

}
