package ru.inshakov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.dto.Domain;
import ru.inshakov.tm.exception.empty.EmptyFilePathException;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class BackupLoadCommand extends AbstractDataCommand {


    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "backup-load";
    }

    @NotNull
    @Override
    public String description() {
        return "load backup from xml";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String filePath = serviceLocator.getPropertyService().getFileXmlPath("backup");
        Optional.ofNullable(filePath).orElseThrow(EmptyFilePathException::new);
        @Nullable final File file = new File(filePath);
        if (!file.exists()) return;
        System.out.println(filePath);
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(filePath)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}

