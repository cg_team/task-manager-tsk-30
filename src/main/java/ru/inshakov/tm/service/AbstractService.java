package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.entity.EntityNotFoundException;
import ru.inshakov.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(final @NotNull E entity) {
        repository.add(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void addAll(@Nullable List<E> entity) {
        repository.addAll(entity);
    }

    @Override
    public @Nullable List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public @Nullable E findById(@NotNull String id) {
        return repository.findById(id);
    }

    @Override
    public void remove(final @NotNull E entity) {
        repository.remove(entity);
    }

    @Override
    public @Nullable E removeById(@NotNull String id) {
        return repository.removeById(id);
    }

}


